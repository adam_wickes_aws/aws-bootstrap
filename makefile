RUNNER=docker-compose run --rm
RUNNER-TF=$(RUNNER) terraform
ENV=main
VARS=vars/$(ENV).tfvars
TF_BACKEND_DYNAMO=anwickes-personal-tfstate-lock
TF_BACKEND_BUCKET=anwickes-personal-tfstate

.env:
	cp .env.template .env
	@echo AWS_DEFAULT_REGION=ap-southeast-2 >> .env
	@echo AWS_ACCESS_KEY_ID=$(AWS_ACCESS_KEY_ID) >> .env
	@echo AWS_SECRET_ACCESS_KEY=$(AWS_SECRET_ACCESS_KEY) >> .env
.PHONY: .env

init: .env
	$(RUNNER-TF) terraform init \
		-backend-config="dynamodb_table=$(TF_BACKEND_DYNAMO)" \
		-backend-config="bucket=$(TF_BACKEND_BUCKET)" \
		-backend-config="key=$(ENV)"

plan: .env init
	$(RUNNER-TF) terraform plan -var-file=$(VARS) -out=.tf-plan -lock=false

apply: .env init
	$(RUNNER-TF) terraform apply .tf-plan

import: .env init
	$(RUNNER-TF) terraform import aws_acm_certificate.adamwickes_dev arn:aws:acm:us-east-1:302127924429:certificate/1eafc589-fdcd-471d-a0b9-461bccf35116