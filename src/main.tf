# Require TF version to be same as or greater than 1.2.3
terraform {
  required_version = ">=1.2.3"
  backend "s3" {
    region         = "ap-southeast-2"
  }
  required_providers {
    aws = {
      version = "~> 4.21.0"
    }
  }
}

provider "aws" {
  region  = "ap-southeast-2"
}

provider "aws" {
  alias  = "east"
  region = "us-east-1"
}

# Call the seed_module to build our ADO seed info
module "bootstrap" {
  source                      = "./modules/bootstrap"
  name_of_s3_bucket           = "anwickes-personal-tfstate"
  dynamo_db_table_name        = "anwickes-personal-tfstate-lock"
}
