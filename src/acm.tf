# cert for adamwickes.dev
resource "aws_acm_certificate" "adamwickes_dev" {
    provider = aws.east
    domain_name                 = "adamwickes.dev"
    subject_alternative_names   = toset(["www.adamwickes.dev"])
    validation_method           = "DNS"

    tags = {
        deployment = "clickops",
        repository = "aws-bootstrap"
    }

    lifecycle {
        create_before_destroy = true
    }
}